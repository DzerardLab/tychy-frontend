const FONT_SIZES_CLASS = [ 'font-small', 'font-medium', 'font-big'];

class App {
  constructor() {
    this.$fontSizeContainer = $('.scroller-content');
    this.init();
    this.changeSize();
    this.setupBackground();
    this.clickActiveTopic();
  }

  changeSize() {
    const $sizeChanger = $('.js-change-font-size');
    const values = FONT_SIZES_CLASS;
    const valuesLength = FONT_SIZES_CLASS.length;
    let currentIndex = 0;

    $sizeChanger.on('click', e => {
      if(!this.$fontSizeContainer.length) {
        console.log('Please point/add container to change font size ...');
        return false;
      }

      let myClass = this.$fontSizeContainer.attr('class');
      let $btn = $(e.currentTarget);

      FONT_SIZES_CLASS.find((element, index) => {
        if(myClass.search(element) > -1) {
          currentIndex = index;
        }
      });

      this.$fontSizeContainer.removeClass(FONT_SIZES_CLASS.join(' '));
      $btn.find('span').removeClass('active');

      currentIndex++;

      if(currentIndex >= valuesLength) {
        currentIndex = 0;
      }

      this.$fontSizeContainer.addClass(FONT_SIZES_CLASS[currentIndex]);
      $btn.find(`.${FONT_SIZES_CLASS[currentIndex]}`).addClass('active');

      this.$fontSizeContainer.scrollTop(0);
      this.$fontSizeContainer.perfectScrollbar('update');
    });
  }

  setupBackground() {

    // Background size 1920x540
    const $topImageSize = {
      width: 1920,
      height: 670,
      defaultHeight: 1080
    };

    const CUSTOM_GUTTER_RADIUS = 60;
    const ITEM_HEIGHT = 150;

    const $main = $('main');
    const $topicScreen  = $('.body-choose-topic');
    let $activeItem = $topicScreen.find('li.active');

    if(!$topicScreen.length) {
      return;
    }

    let params = {
      offset: $activeItem.offset(),
      width: $activeItem.outerWidth(),
      height: $activeItem.outerHeight()
    };

    $topicScreen.find('ul').css('background-position', `0px -${$topImageSize.defaultHeight - $main.outerHeight() + ITEM_HEIGHT}px`);

    $activeItem.css('background-position', `${$topImageSize.width - params.offset.left}px ${ITEM_HEIGHT}px`);

    let $afterClipBottom = $activeItem.find('.clip-after .clip-bottom');
    $afterClipBottom.css('background-position', `${$topImageSize.width - $afterClipBottom.offset().left}px ${ITEM_HEIGHT}px`);

    let $beforeClipBottom = $activeItem.find('.clip-before .clip-bottom');
    $beforeClipBottom.css('background-position', `${$topImageSize.width - $beforeClipBottom.offset().left}px ${ITEM_HEIGHT}px`);

    let $afterClipTop = $activeItem.find('.clip-after .clip-top');
    $afterClipTop.css('background-position', `${$topImageSize.width - $afterClipBottom.offset().left - 12}px ${$topImageSize.defaultHeight - $main.outerHeight() + ITEM_HEIGHT}px`);

    let $beforeClipTop = $activeItem.find('.clip-before .clip-top');
    $beforeClipTop.css('background-position', `${$topImageSize.width - $beforeClipBottom.offset().left}px ${$topImageSize.defaultHeight - $main.outerHeight() + ITEM_HEIGHT}px`);

    //draw lines above list
    $('.nav-line-before').width(params.offset.left - CUSTOM_GUTTER_RADIUS);
    $('.nav-line-after').width($main.innerWidth() - params.offset.left - CUSTOM_GUTTER_RADIUS - params.width);

    //loaded
    $topicScreen.removeClass('invisible');
    //bind click as example
  }

  clickActiveTopic() {
    const $topicScreen  = $('.body-choose-topic');

    $topicScreen.find('li').on('click', (e) => {
      let $element = $(e.currentTarget);

      $element.siblings().removeClass('active');
      $element.addClass('active');

      this.setupBackground();
      return false;
    });

    $topicScreen.find('.clip-before').on('click', (e) => {
      let $element = $(e.currentTarget).parent();

      if($element.prev().length) {
        $element.prev().siblings().removeClass('active');
        $element.prev().addClass('active');
      }

      this.setupBackground();
      return false;
    });

    $topicScreen.find('.clip-after').on('click', (e) => {
      let $element = $(e.currentTarget).parent();

      if($element.next().length) {
        $element.next().siblings().removeClass('active');
        $element.next().addClass('active');
      }

      this.setupBackground();
      return false;
    });

    window.addEventListener('resize', () => {
      this.setupBackground();
    }, false);
  }

  init() {
    this.$fontSizeContainer.perfectScrollbar({
      theme: 'new-ams'
    });

    $('.nav-left li').on('click', (e) => {
      let $element = $(e.currentTarget);
      $element.siblings().removeClass('active');
      $element.addClass('active');

      return false;
    });

    const $presentationButtons = $('.presentation-buttons-wrapper button');
    $presentationButtons.on('click', (e) => {
      let $element = $(e.currentTarget);
      $presentationButtons.removeClass('active');
      $element.addClass('active');

      return false;
    });

    $('.btn-bg').on('click', (e) => {
      $(e.currentTarget).toggleClass('active');
    });

    $('.btn-switch-lang').on('click', (e) => {
      //$(e.currentTarget).toggleClass('active');
      window.location.reload();
      console.log('When clicked redirect?');
    })
  }
}
new App();
