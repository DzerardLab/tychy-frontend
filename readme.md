Schemes for project:

Definition in *app/styles/_variables.scss*

Currently avaialble schemes are: 
'red-scheme', 'blue-scheme', 'orange-scheme', 'grey-scheme', 'green-scheme'

Simply just add class ex. 'red-scheme' to body classes. 
<body class="red-scheme"> content ... </body>
